package atm.dis.authserv1.resourceserv.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @GetMapping("/api/user")
    @PreAuthorize("hasAuthority('PRIVILEGE_USER_READ')")
    public String user() {
        return "{\"message\" : \"Just user\"}";
    }
}
