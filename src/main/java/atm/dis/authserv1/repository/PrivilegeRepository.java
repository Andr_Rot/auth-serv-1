package atm.dis.authserv1.repository;

import atm.dis.authserv1.model.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {

}
