package atm.dis.authserv1.resourceserv.service;

import atm.dis.authserv1.model.Account;
import atm.dis.authserv1.repository.AccountRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class AdminService {
    
    private final AccountRepository accountRepository;

    public AdminService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }
    
    public  List<Account> getUserList() {
        return accountRepository.findAll();

    }
}
