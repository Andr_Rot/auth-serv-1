package atm.dis.authserv1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthServ1Application {

	public static void main(String[] args) {
		SpringApplication.run(AuthServ1Application.class, args);
	}
}
