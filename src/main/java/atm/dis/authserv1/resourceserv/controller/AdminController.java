package atm.dis.authserv1.resourceserv.controller;

import atm.dis.authserv1.model.Account;
import atm.dis.authserv1.resourceserv.service.AdminService;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {
    
    private final AdminService adminService;

    public AdminController(final AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping("/api/admin")
    @PreAuthorize("hasAuthority('PRIVILEGE_ADMIN_READ')")
    public List<Account> admin() {
        
        return adminService.getUserList();        
//        return "{\"message\" : \"Just admin\"}";
    }

    @GetMapping("/test")
    public String test() {
        System.out.println("test");
        return "{\"message\" : \"Just test\"}";
    }
}
