package atm.dis.authserv1.repository;

import atm.dis.authserv1.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
