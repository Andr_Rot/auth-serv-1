package atm.dis.authserv1.repository;

import atm.dis.authserv1.model.Account;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findByUsername(final String username);
}
