INSERT INTO privilege (privilege_id, name, description) VALUES (1, 'PRIVILEGE_ADMIN_READ', 'description - privilege admin read');
INSERT INTO privilege (privilege_id, name, description) VALUES (2, 'PRIVILEGE_USER_READ', 'description - privilege user read');

INSERT INTO role (role_id, name, description) VALUES (1, 'ROLE_ADMIN', 'description - role admin');
INSERT INTO role (role_id, name, description) VALUES (2, 'ROLE_USER', 'description - role user');

INSERT INTO role_privileges (id, role_role_id, privileges_privilege_id) VALUES (1, 1, 1);
INSERT INTO role_privileges (id, role_role_id, privileges_privilege_id) VALUES (2, 2, 2);

INSERT INTO account (id, enabled, username, password) VALUES (1, true, 'admin', '$2a$10$MTFVrdqbHOi.CCUhkrkZnOBdrZEfk3gzIUyZBdQvLWvdF/0pnkEO2');
INSERT INTO account (id, enabled, username, password) VALUES (2, true, 'user', '$2a$10$6KDklkImZgGANWR8pDAwSexf6Bt4Z9I0nDiwdih9Q38HI4eAkWk0u');

INSERT INTO account_roles (id, account_id, roles_role_id) VALUES (1, 1, 1);
INSERT INTO account_roles (id, account_id, roles_role_id) VALUES (2, 2, 2);